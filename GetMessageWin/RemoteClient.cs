﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace GetMessageWin
{
    class RemoteClient
    {
        private TcpClient client;
        private NetworkStream streamToClient;
        private NetworkStream streamFileToClient;
        private const int BufferSize = 8192;
        private const int ServerFileBuffer = 1024*1024;
        private byte[] buffer;
        private RequestHandler handler;
        private ProtocolHandler handlerPro;
        private AntCommand ac;
        private string TestMode="";
        public RemoteClient(TcpClient client)
        {
            this.client = client;
            ac = new AntCommand();
            // 打印连接到的客户端信息
            Console.WriteLine("[ServerMessage] Client Connected！{0} <-- {1}",
            client.Client.LocalEndPoint, client.Client.RemoteEndPoint);

            // 获得流
            streamToClient = client.GetStream();
            streamFileToClient = client.GetStream();
            buffer = new byte[BufferSize];

            // 设置RequestHandler
            handler = new RequestHandler();
            handlerPro = new ProtocolHandler();
            // 在构造函数中就开始准备读取
            //AsyncCallback callBack = new AsyncCallback(ReadComplete);
            //streamToClient.BeginRead(buffer, 0, BufferSize, callBack, null);
        }
        // 开始进行读取
        public void BeginRead()
        {
            AsyncCallback callBack = new AsyncCallback(OnReadComplete);
            streamFileToClient.BeginRead(buffer, 0, BufferSize, callBack, null);
        }
        // 再读取完成时进行回调
        private void OnReadComplete(IAsyncResult ar)
        {
            int bytesRead = 0;
            try
            {
                lock (streamToClient)
                {
                    bytesRead = streamToClient.EndRead(ar);
                    Console.WriteLine("[ServerMessage] Reading data, {0} bytes ...", bytesRead);
                }
                if (bytesRead == 0) throw new Exception("读取到0字节");

                string msg = Encoding.Unicode.GetString(buffer, 0, bytesRead);
                Array.Clear(buffer, 0, buffer.Length);        // 清空缓存，避免脏读

                // 获取protocol数组           
                if(handlerPro.GetProtocol(msg).Length==0)
                {
                    string back = msg.ToUpper();
                    //接收到命令后发送命令                  
                    ac.MyCommand(back);
                    // 将得到的字符串改为大写并重新发送
                    byte[] temp = Encoding.Unicode.GetBytes("[ServerMessage] Command:<" + back + "> send successful");
                    streamToClient.Write(temp, 0, temp.Length);
                    streamToClient.Flush();
                    Console.WriteLine("[ServerMessage] Sent: {0}", back);
                }
                else
                {
                    string[] protocolArray = handlerPro.GetProtocol(msg);
                    //处理文件
                    foreach (string pro in protocolArray)
                    {
                        // 这里异步调用，不然这里可能会比较耗时
                        ParameterizedThreadStart start = new ParameterizedThreadStart(handleProtocol);
                        start.BeginInvoke(pro, null, null);
                    }
                }

                // 再次调用BeginRead()，完成时调用自身，形成无限循环
                lock (streamToClient)
                {
                    AsyncCallback callBack = new AsyncCallback(OnReadComplete);
                    streamToClient.BeginRead(buffer, 0, BufferSize, callBack, null);
                }
            }
            catch (Exception ex)
            {
                if (streamToClient != null)
                    streamToClient.Dispose();
                client.Close();
                Console.WriteLine(ex.Message);      // 捕获异常时退出程序
            }
        }
        // 处理protocol
        private void handleProtocol(object obj)
        {
            string pro = obj as string;
            ProtocolHelper helper = new ProtocolHelper(pro);
            FileProtocol protocol = helper.GetProtocol();

            if (protocol.Mode == FileRequestMode.Send)
            {
                // 客户端发送文件，对服务端来说则是接收文件
                receiveFile(protocol);
            }
            else if (protocol.Mode == FileRequestMode.Receive)
            {
                // 客户端接收文件，对服务端来说则是发送文件
                sendFile(protocol);
            }
        }
        // 发送文件
        private void sendFile(FileProtocol protocol)
        {
            TcpClient localClient;
            NetworkStream streamToClient = getStreamToClient(protocol, out localClient);

            // 获得文件的路径
            string filePath = Environment.CurrentDirectory + "/" + protocol.FileName;

            // 创建文件流
            FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            byte[] fileBuffer = new byte[ServerFileBuffer];     // 每次传1KB
            int bytesRead;
            int totalBytes = 0;

            // 创建获取文件发送状态的类
            SendStatus status = new SendStatus(filePath);

            // 将文件流转写入网络流
            try
            {
                do
                {
                    //Thread.Sleep(10);           // 为了更好的视觉效果，暂停10毫秒:-)
                    bytesRead = fs.Read(fileBuffer, 0, fileBuffer.Length);
                    streamToClient.Write(fileBuffer, 0, bytesRead);
                    totalBytes += bytesRead;            // 发送了的字节数
                    status.PrintStatus(totalBytes); // 打印发送状态
                } while (bytesRead > 0);
                Console.WriteLine("[ServerMessage] Total {0} bytes sent, Done!", totalBytes);
            }
            catch
            {
                Console.WriteLine("[ServerMessage] Server has lost...");
            }

            streamToClient.Dispose();
            fs.Dispose();
            localClient.Close();
        }
        // 获取连接到远程的流 -- 公共方法
        private NetworkStream getStreamToClient(FileProtocol protocol, out TcpClient localClient)
        {
            // 获取远程客户端的位置
            IPEndPoint endpoint = client.Client.RemoteEndPoint as IPEndPoint;
            IPAddress ip = endpoint.Address;

            // 使用新端口号，获得远程用于接收文件的端口
            endpoint = new IPEndPoint(ip, protocol.Port);

            // 连接到远程客户端
            try
            {
                localClient = new TcpClient();
                localClient.Connect(endpoint);
            }
            catch
            {
                Console.WriteLine("[ServerMessage] 无法连接到客户端 --> {0}", endpoint);
                localClient = null;
                return null;
            }

            // 获取发送文件的流
            NetworkStream streamToClient = localClient.GetStream();
            return streamToClient;
        }

        private void receiveFile(FileProtocol protocol)
        {
            // 获取远程客户端的位置
            IPEndPoint endpoint = client.Client.RemoteEndPoint as IPEndPoint;
            IPAddress ip = endpoint.Address;

            // 使用新端口号，获得远程用于接收文件的端口
            endpoint = new IPEndPoint(ip, protocol.Port);

            // 连接到远程客户端
            TcpClient localClient;
            try
            {
                localClient = new TcpClient();
                localClient.Connect(endpoint);
            }
            catch
            {
                Console.WriteLine("[ServerMessage] 无法连接到客户端 --> {0}", endpoint);
                return;
            }

            // 获取发送文件的流
            NetworkStream streamToClient = localClient.GetStream();

            // 随机生成一个在当前目录下的文件名称
            //string path = Environment.CurrentDirectory + "/" + generateFileName(protocol.FileName);
            
            if (!Directory.Exists(protocol.FilePathToLcaol+ TestMode))
            {
                Directory.CreateDirectory(protocol.FilePathToLcaol + TestMode);
            }
            else
            {
                Directory.Delete(protocol.FilePathToLcaol + TestMode, true);
                Directory.CreateDirectory(protocol.FilePathToLcaol + TestMode);
            }
            string path = protocol.FilePathToLcaol + TestMode+ "/" + protocol.FileName;
            if(File.Exists(path))
            {
                File.Delete(path);
            }
            while(File.Exists(path))
            { }
            byte[] fileBuffer = new byte[ServerFileBuffer]; // 每次收1KB
            FileStream fs = new FileStream(path, FileMode.CreateNew, FileAccess.Write);

            // 从缓存buffer中读入到文件流中
            int bytesRead;
            int totalBytes = 0;
            do
            {
                bytesRead = streamToClient.Read(buffer, 0, BufferSize);
                fs.Write(buffer, 0, bytesRead);
                totalBytes += bytesRead;
                Console.WriteLine("[ServerMessage] Receiving {0} bytes ...", totalBytes);
            } while (bytesRead > 0);

            Console.WriteLine("[ServerMessage] Total {0} bytes received, Done!", totalBytes);

            streamToClient.Dispose();
            fs.Dispose();
            localClient.Close();
        }
        // 随机获取一个图片名称
        private string generateFileName(string fileName)
        {
            DateTime now = DateTime.Now;
            return String.Format(
            "{0}_{1}_{2}_{3}", now.Minute, now.Second, now.Millisecond, fileName
            );
        }
        // 再读取完成时进行回调
        private void ReadComplete(IAsyncResult ar)
        {
            int bytesRead = 0;
            try
            {
                lock (streamToClient)
                {
                    bytesRead = streamToClient.EndRead(ar);
                    Console.WriteLine("[ServerMessage] Reading data, {0} bytes ...", bytesRead);
                }
                if (bytesRead == 0) throw new Exception("[ClientServer]  读取到0字节");

                string msg = Encoding.Unicode.GetString(buffer, 0, bytesRead);
                Array.Clear(buffer, 0, buffer.Length);        // 清空缓存，避免脏读

                string[] msgArray = handler.GetActualString(msg);   // 获取实际的字符串


                Console.WriteLine("[ServerMessage] Received: {0}", msg);
                string back = msg.ToUpper();
                //接收到命令后发送命令                  
                ac.MyCommand(back);

                // 将得到的字符串改为大写并重新发送
                byte[] temp = Encoding.Unicode.GetBytes("[ServerMessage] Command:<" + back+"> send successful");
                streamToClient.Write(temp, 0, temp.Length);
                streamToClient.Flush();
                Console.WriteLine("[ServerMessage] Sent: {0}", back);

                // 再次调用BeginRead()，完成时调用自身，形成无限循环
                lock (streamToClient)
                {
                    AsyncCallback callBack = new AsyncCallback(ReadComplete);
                    streamToClient.BeginRead(buffer, 0, BufferSize, callBack, null);
                }
            }
            catch (Exception ex)
            {
                if (streamToClient != null)
                    streamToClient.Dispose();
                client.Close();
                Console.WriteLine(ex.Message);      // 捕获异常时退出程序              
            }
        }
    }
}
