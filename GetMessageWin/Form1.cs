﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace GetMessageWin
{
    public partial class Form1 : Form
    {
        public static string IPForLocal = "";
        public Form1()
        {
            InitializeComponent();
            IPAddress[] addressList = Dns.GetHostByName(Dns.GetHostName()).AddressList;

            foreach (IPAddress item in addressList)
            {
                Console.WriteLine(item.ToString());
                IPForLocal = item.ToString();
            }
            Console.WriteLine("Using IP:"+ IPForLocal);
        }
        private Thread[] Tools_Build_Thread = new Thread[10];
        private void Form1_Load(object sender, EventArgs e)
        {
            Tools_Build_Thread[0] = new Thread(new ThreadStart(() => ServerStart("")));
            Tools_Build_Thread[0].Start();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Tools_Build_Thread[1] = new Thread(new ThreadStart(() => ClientStart("")));
            Tools_Build_Thread[1].Start();
        }
        public void ServerStart(string s)
        {
            Console.WriteLine("Server is running ... ");
            IPAddress ip = IPAddress.Parse(IPForLocal);
            // 开启对控制端口 8500 的侦听
            TcpListener listener = new TcpListener(ip, 8500);
            listener.Start();
            while (true)
            {
                // 获取一个连接，同步方法，在此处中断
                TcpClient client = listener.AcceptTcpClient();
                RemoteClient wapper = new RemoteClient(client);
                wapper.BeginRead();
            }
            //Kill Thread
            if (Tools_Build_Thread[0].IsAlive)
            {
                Tools_Build_Thread[0].Abort();
            }
        }
        private ServerClient AntClient;
        public string ServerAntPackIP = "127.0.0.1";
        public int ServerAntPackPort = 8500;

        public void ClientStart(string s)
        {
            ServerAntPackIP = textBox1.Text;
            ServerClient AntClient = new ServerClient(ServerAntPackIP, ServerAntPackPort);
            AntClient.ConnectServer_AntPack(ServerAntPackIP, ServerAntPackPort);
            //AntClient.SendCommand_AntPack("build");
            //AntClient.SendFile_AntPack(@"C:\Users\Qin\Desktop\1\AndroidManifest.xml");
            AntClient.SendFile_AntPack(@textBox2.Text);


        }
        public void SendFile_AntPack(string _FileLocation)
        {
            if (AntClient != null)
            {
                if (File.Exists(_FileLocation))
                    AntClient.BeginSendFile(_FileLocation);
                else
                    Console.WriteLine("[SendCommandAntPack] Connect Failed");
            }
            else
            {
                Console.WriteLine("[SendFile_AntPack] Connect Failed");
            }
        }
        public void SendCommand_AntPack(string _Command)
        {
            if (AntClient != null)
            {
                AntClient.SendMessage(_Command);             
            }
            else
            {
                Console.WriteLine("[SendCommandAntPack] Connect Failed");
            }
        }
       
        public void ConnectServer_AntPack(string ip, int port)
        {
            ServerClient Client = new ServerClient(ip, port);     
            if (Client.msg == "")
            {
                AntClient = Client;
                Console.WriteLine("[ConnectServer_AntPack] Connect Server Success");
            }
            else
            {
                MessageBox.Show("Connect Server Failed");
                Console.WriteLine("[ConnectServer_AntPack] onnect Server Failed");
            }
        }
    }
}
