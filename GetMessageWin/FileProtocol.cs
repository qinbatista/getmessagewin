﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static GetMessageWin.ProtocolHandler;

namespace GetMessageWin
{
    class FileProtocol
    {
        private readonly FileRequestMode mode;
        private readonly int port;
        private readonly string fileName;
        private readonly string filePath="";

        public FileProtocol
        (FileRequestMode mode, int port, string fileName,string filePath)
        {
            this.mode = mode;
            this.port = port;
            this.fileName = fileName;
            this.filePath = filePath;
        }

        public FileRequestMode Mode
        {
            get { return mode; }
        }

        public int Port
        {
            get { return port; }
        }

        public string FileName
        {
            get { return fileName; }
        }
        public string FilePath
        {
            get { return filePath; }
        }
        public string FilePathToLcaol
        {
            get
            {
              return @"C:\Users\" + Environment.UserName+@"\"+this.filePath.Substring(this.filePath.IndexOf("Desktop"));
            }
        }
        public override string ToString()
        {
            return String.Format("<protocol><file name=\"{0}\" mode=\"{1}\" port=\"{2}\" path=\"{3}\" /></protocol>", fileName, mode, port, filePath);
        }
    }
}
