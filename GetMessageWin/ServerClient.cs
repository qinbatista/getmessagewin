﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GetMessageWin
{
    class ServerClient : IDisposable
    {
        private const int BufferSize = 8192;
        private const int CliectFileBuffer = 1024*1024;
        private byte[] buffer;
        private TcpClient client;
        private NetworkStream streamToServer;
        public string msg = "";
        private string ServerClientIP;
        private int ServerClientPort;
        private ServerClient AntClient;
        public ServerClient(string ServerIP, int port)
        {
            ServerClientIP = ServerIP;
            ServerClientPort = port;
            try
            {
                client = new TcpClient();
                client.Connect(ServerIP, port);      // 与服务器连接
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                msg = ex.Message;
                return;
            }
            buffer = new byte[BufferSize];

            // 打印连接到的服务端信息
            Console.WriteLine("[ClientMessage] Server Connected！{0} --> {1}",
            client.Client.LocalEndPoint, client.Client.RemoteEndPoint);
            streamToServer = client.GetStream();

            lock (streamToServer)
            {
                AsyncCallback callBack = new AsyncCallback(ReadComplete);
                streamToServer.BeginRead(buffer, 0, BufferSize, callBack, null);
            }

        }
        public void ConnectServer_AntPack(string ip, int port)
        {
            ServerClient Client = new ServerClient(ip, port);
            if (Client.msg == "")
            {
                AntClient = Client;
                Console.WriteLine("[ConnectServer_AntPack] Connect Server Success");
            }
            else
            {                
                Console.WriteLine("[ConnectServer_AntPack] onnect Server Failed");
            }
        }
        public void SendFile_AntPack(string _FileLocation)
        {
            if (AntClient != null)
            {
                if (File.Exists(_FileLocation))
                    AntClient.BeginSendFile(_FileLocation);
                else
                    Console.WriteLine("[SendCommandAntPack] "+ _FileLocation+" is not exsit");
            }
            else
            {
                Console.WriteLine("[SendFile_AntPack] Connect Failed");
            }
        }
        public void SendCommand_AntPack(string _Command)
        {
            if (AntClient != null)
            {
                AntClient.SendMessage(_Command);
            }
            else
            {
                Console.WriteLine("[SendCommandAntPack] Connect Failed");
            }
        }
        // 发送消息到服务端
        public void SendMessage(string msg)
        {
            byte[] temp = Encoding.Unicode.GetBytes(msg);   // 获得缓存
            try
            {
                lock (streamToServer)
                {
                    streamToServer.Write(temp, 0, temp.Length); // 发往服务器
                }
                Console.WriteLine("[ClientMessage] Sent: {0}", msg);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }
        }
        // 发送文件 - 异步方法
        public void BeginSendFile(string filePath)
        {
            ParameterizedThreadStart start =new ParameterizedThreadStart(BeginSendFile);
            start.BeginInvoke(filePath, null, null);
        }

        private void BeginSendFile(object obj)
        {
            string filePath = obj as string;
            SendFile(filePath);
        }
        // 发送文件 -- 同步方法
        public void SendFile(string filePath)
        {

            try
            {
                IPAddress ip = IPAddress.Parse(Form1.IPForLocal);
                TcpListener listener = new TcpListener(ip, 33366);
                listener.Start();
           
            // 获取本地侦听的端口号
            IPEndPoint endPoint = listener.LocalEndpoint as IPEndPoint;
            int listeningPort = endPoint.Port;

            // 获取发送的协议字符串
            string fileName = Path.GetFileName(filePath);
            string filePathName=Path.GetDirectoryName(filePath);
            FileProtocol protocol =
            new FileProtocol(FileRequestMode.Send, listeningPort, fileName, filePathName);
            string pro = protocol.ToString();

            SendMessage(pro);       // 发送协议到服务端

            // 中断，等待远程连接
            TcpClient localClient = listener.AcceptTcpClient();
            Console.WriteLine("[ClientMessage] Start sending file...");
            NetworkStream stream = localClient.GetStream();

            // 创建文件流
            FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            byte[] fileBuffer = new byte[CliectFileBuffer];     // 每次传1KB
            int bytesRead;
            int totalBytes = 0;

            // 创建获取文件发送状态的类
            SendStatus status = new SendStatus(filePath);

            // 将文件流转写入网络流
            try
            {
                do
                {
                    //Thread.Sleep(10);           // 为了更好的视觉效果，暂停10毫秒:-)
                    bytesRead = fs.Read(fileBuffer, 0, fileBuffer.Length);
                    stream.Write(fileBuffer, 0, bytesRead);
                    totalBytes += bytesRead;            // 发送了的字节数
                    status.PrintStatus(totalBytes); // 打印发送状态
                } while (bytesRead > 0);
                //Console.WriteLine("[ClientMessage] Total {0} bytes sent, Done!", totalBytes);
            }
            catch
            {
                Console.WriteLine("[ClientMessage] Server has lost...");
            }

            stream.Dispose();
            fs.Dispose();
            localClient.Close();
            listener.Stop();
            }
            catch (Exception e)
            {
                Console.WriteLine("e=" + e.ToString());
            }
        }
        public void SendMessage()
        {
            SendMessage(this.msg);
        }
        // 接收文件 -- 异步方法
        public void BeginReceiveFile(string fileName)
        {
            ParameterizedThreadStart start =
            new ParameterizedThreadStart(ReceiveFile);
            start.BeginInvoke(fileName, null, null);
        }

        public void ReceiveFile(object obj)
        {
            string fileName = obj as string;
            ReceiveFile(fileName);
        }
        // 接收文件 -- 同步方法
        public void ReceiveFile(string fileName)
        {

            IPAddress ip = IPAddress.Parse(ServerClientIP);
            TcpListener listener = new TcpListener(ip, 0);
            listener.Start();

            // 获取本地侦听的端口号
            IPEndPoint endPoint = listener.LocalEndpoint as IPEndPoint;
            int listeningPort = endPoint.Port;

            // 获取发送的协议字符串
            FileProtocol protocol =new FileProtocol(FileRequestMode.Receive, listeningPort, fileName,"");
            string pro = protocol.ToString();

            SendMessage(pro);       // 发送协议到服务端

            // 中断，等待远程连接
            TcpClient localClient = listener.AcceptTcpClient();
            Console.WriteLine("[ClientMessage] Start sending file...");
            NetworkStream stream = localClient.GetStream();

            // 获取文件保存的路劲
            string filePath =
            Environment.CurrentDirectory + "/" + generateFileName(fileName);

            // 创建文件流
            FileStream fs = new FileStream(filePath, FileMode.CreateNew, FileAccess.Write);
            byte[] fileBuffer = new byte[CliectFileBuffer];     // 每次传1KB
            int bytesRead;
            int totalBytes = 0;

            // 从缓存buffer中读入到文件流中
            do
            {
                bytesRead = stream.Read(buffer, 0, BufferSize);
                fs.Write(buffer, 0, bytesRead);
                totalBytes += bytesRead;
                Console.WriteLine("[ClientMessage]  Receiving {0} bytes ...", totalBytes);
            } while (bytesRead > 0);

            Console.WriteLine("[ClientMessage] Total {0} bytes received, Done!", totalBytes);

            fs.Dispose();
            stream.Dispose();
            localClient.Close();
            listener.Stop();
        }

        // 读取完成时的回调方法
        private void ReadComplete(IAsyncResult ar)
        {
            int bytesRead;

            try
            {
                lock (streamToServer)
                {
                    bytesRead = streamToServer.EndRead(ar);
                }
                if (bytesRead == 0) throw new Exception("[ClientMessage] 读取到0字节");

                string msg = Encoding.Unicode.GetString(buffer, 0, bytesRead);
                Console.WriteLine("[ClientMessage] Received: {0}", msg);
                Array.Clear(buffer, 0, buffer.Length);      // 清空缓存，避免脏读

                lock (streamToServer)
                {
                    AsyncCallback callBack = new AsyncCallback(ReadComplete);
                    streamToServer.BeginRead(buffer, 0, BufferSize, callBack, null);
                }
            }
            catch (Exception ex)
            {
                if (streamToServer != null)
                    streamToServer.Dispose();
                client.Close();

                Console.WriteLine(ex.Message);
            }
        }
        private string generateFileName(string fileName)
        {
            DateTime now = DateTime.Now;
            return String.Format(
            "{0}_{1}_{2}_{3}", now.Minute, now.Second, now.Millisecond, fileName
            );
        }

        void IDisposable.Dispose()
        {
            if (streamToServer != null)
                streamToServer.Dispose();
            if (client != null)
                client.Close();
            throw new NotImplementedException();
        }
    }
}
